package ranglist

import (
	"testing"
)

func TestRangeList(t *testing.T) {
	type args struct {
		op           func(rangeElement [2]int) error
		rangeElement [2]int
	}
	rangeList := RangeList{}
	tests := []struct {
		name    string
		args    []args
		wantErr bool
		strs    []string
	}{
		{
			name: "invalid Add params",
			args: []args{
				{op: rangeList.Add, rangeElement: [2]int{2, 1}},
			},
			wantErr: true,
		},
		{
			name: "invalid Remove params",
			args: []args{
				{op: rangeList.Remove, rangeElement: [2]int{2, 1}},
			},
			wantErr: true,
		},
		{
			name: "normal",
			args: []args{
				{op: rangeList.Add, rangeElement: [2]int{3, 4}},
				{op: rangeList.Add, rangeElement: [2]int{1, 2}},
				{op: rangeList.Add, rangeElement: [2]int{2, 3}},
				{op: rangeList.Add, rangeElement: [2]int{1, 5}},
				{op: rangeList.Add, rangeElement: [2]int{10, 20}},
				{op: rangeList.Add, rangeElement: [2]int{20, 20}},
				{op: rangeList.Add, rangeElement: [2]int{20, 21}},
				{op: rangeList.Add, rangeElement: [2]int{2, 4}},
				{op: rangeList.Add, rangeElement: [2]int{3, 8}},
				{op: rangeList.Remove, rangeElement: [2]int{10, 10}},
				{op: rangeList.Remove, rangeElement: [2]int{10, 11}},
				{op: rangeList.Remove, rangeElement: [2]int{15, 17}},
				{op: rangeList.Remove, rangeElement: [2]int{3, 19}},
				{op: rangeList.Remove, rangeElement: [2]int{20, 21}},
				{op: rangeList.Remove, rangeElement: [2]int{-1, 21}},
			},
			wantErr: false,
			strs: []string{
				"[3, 4)",
				"[1, 2) [3, 4)",
				"[1, 4)",
				"[1, 5)",
				"[1, 5) [10, 20)",
				"[1, 5) [10, 20)",
				"[1, 5) [10, 21)",
				"[1, 5) [10, 21)",
				"[1, 8) [10, 21)",
				"[1, 8) [10, 21)",
				"[1, 8) [11, 21)",
				"[1, 8) [11, 15) [17, 21)",
				"[1, 3) [19, 21)",
				"[1, 3) [19, 20)",
				"",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for i, arg := range tt.args {
				if err := arg.op(arg.rangeElement); (err != nil) != tt.wantErr {
					t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
				}

				if tt.wantErr {
					continue
				}

				if str := rangeList.String(); str != tt.strs[i] {
					t.Errorf("step[%d]: String() str = %v, want str %v", i, str, tt.strs[i])
				}
			}
		})
	}
}

func BenchmarkRangeList(b *testing.B) {
	rl := RangeList{}
	for n := 1; n < b.N; n++ {
		rl.Add([2]int{n, n + 1})
		rl.Remove([2]int{n - 1, n})
	}
}
