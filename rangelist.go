package ranglist

import (
	"errors"
	"fmt"
	"strings"

	"github.com/huandu/skiplist"
)

var (
	ErrInvalidRangeElement = errors.New("invalid range element")
)

// RangeList is a pair of integers define a range
type RangeList struct {
	list *skiplist.SkipList
}

// Add adds range
func (rangeList *RangeList) Add(rangeElement [2]int) error {
	start, end, err := rangeList.checkRangeElement(rangeElement)
	if err != nil {
		return err
	}
	if end < start {
		return nil
	}
	if rangeList.list == nil {
		rangeList.list = skiplist.New(skiplist.Int)
	}

	has, next := rangeList.hasIntersection(start, end)
	if !has {
		rangeList.insert(start, end)
		return nil
	}

	return rangeList.insertByMerge(next, start, end)
}

// Remove removes range
func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	start, end, err := rangeList.checkRangeElement(rangeElement)
	if err != nil {
		return err
	}
	if end < start {
		return nil
	}
	if rangeList.list == nil {
		rangeList.list = skiplist.New(skiplist.Int)
	}

	has, next := rangeList.hasIntersection(start, end)
	if !has {
		return nil
	}

	return rangeList.deleteByMerge(next, start, end)
}

// Print prints the string of RangeList
func (rangeList *RangeList) Print() error {
	fmt.Println(rangeList.String())
	return nil
}

// String returns the string in format [1, 3) [4, 5) [100, 200)
func (rangeList *RangeList) String() string {
	eles := make([]string, 0, rangeList.list.Len())
	ele := rangeList.list.Front()
	for ; ele != nil; ele = ele.Next() {
		eles = append(eles, fmt.Sprintf("[%d, %d)", int(ele.Score()), ele.Value.(int)+1))
	}
	return strings.Join(eles, " ")
}

// hasIntersection detects the [start, end] has intersection with the RangeList,
// returns the next range bigger than or equals start
func (rangeList *RangeList) hasIntersection(start, end int) (bool, *skiplist.Element) {
	next := rangeList.list.Find(start)
	if rangeList.list.Front() == nil {
		return false, nil
	}
	noIntersection := end < int(rangeList.list.Front().Score()) || // bigger than front
		start > rangeList.list.Back().Value.(int) || // bigger than back
		(next != nil && end < int(next.Score()) && // bigger than pre and smaller than next
			(next.Prev() == nil || start > next.Prev().Value.(int)))
	return !noIntersection, next
}

// insert inserts the range [start, end] withdraw intersection,
// and concat the previous and next range if these continuous
func (rangeList *RangeList) insert(start, end int) {
	next := rangeList.list.Find(end)
	pre := rangeList.list.Back()
	if next != nil && next.Prev() != nil {
		pre = next.Prev()
	}
	// concat with pre element
	if pre != nil && pre.Value.(int) == start-1 &&
		(next == nil || int(next.Score()) != end+1) {
		rangeList.list.Set(int(pre.Score()), end)
		return
	} else if (pre == nil || pre.Value.(int) != start-1) &&
		next != nil && int(next.Score()) == end+1 {
		// concat with next element
		rangeList.list.Set(start, next.Value)
		rangeList.list.Remove(int(next.Score()))
	} else if pre != nil && pre.Value.(int) == start-1 &&
		next != nil && int(next.Score()) == end+1 {
		// concat with pre hand next elements
		rangeList.list.Set(int(pre.Score()), next.Value)
		rangeList.list.Remove(int(next.Score()))
	} else {
		// without concat
		rangeList.list.Set(start, end)
	}
}

// insertByMerge inserts the range [start, end]
func (rangeList *RangeList) insertByMerge(next *skiplist.Element, start, end int) error {
	first := rangeList.firstToMerge(next, start)
	if first != nil {
		start = int(first.Score())
	} else {
		first = next
	}

	toRemoveEleScores := make([]int, 0)
	for ; first != nil; first = first.Next() {
		if end < int(first.Score()) {
			break
		}

		toRemoveEleScores = append(toRemoveEleScores, int(first.Score()))
		if end <= first.Value.(int) {
			end = first.Value.(int)
			break
		}
	}
	for _, s := range toRemoveEleScores {
		rangeList.list.Remove(s)
	}
	rangeList.list.Set(start, end)
	return nil
}

// deleteByMerge deletes range [start, end]
func (rangeList *RangeList) deleteByMerge(next *skiplist.Element, start, end int) error {
	first := rangeList.firstToMerge(next, start)
	if first == nil {
		first = next
	}

	toAddElements := make([][2]int, 0)
	toRemoveEleScores := make([]int, 0)
	for ; first != nil; first = first.Next() {
		// remove the sub range
		if start <= int(first.Score()) && first.Value.(int) <= end {
			toRemoveEleScores = append(toRemoveEleScores, int(first.Score()))
			continue
		}
		// remove the sub head element
		if start <= int(first.Score()) {
			rangeList.list.Remove(first.Score())
		} else { // truncate head element
			toAddElements = append(toAddElements, [2]int{int(first.Score()), start - 1})
		}
		// new element tail element
		if end < first.Value.(int) {
			toAddElements = append(toAddElements, [2]int{end + 1, first.Value.(int)})
			break
		}
	}
	for _, s := range toRemoveEleScores {
		rangeList.list.Remove(s)
	}
	for _, s := range toAddElements {
		rangeList.list.Set(s[0], s[1])
	}
	return nil
}

// firstToMerge finds the first element contains the start, returns a nil when start not in range.
func (rangeList *RangeList) firstToMerge(next *skiplist.Element, start int) *skiplist.Element {
	if next == nil {
		return rangeList.list.Back()
	}
	var first *skiplist.Element
	if int(next.Score()) == start {
		first = next
	} else if pre := next.Prev(); pre != nil && start <= pre.Value.(int) {
		first = pre
	}
	return first
}

// checkRangeElement checks the range elements, rangeElements[1] must be bigger or equal than rangeElements[0]
// returns the then start and end in the range.
func (rangeList *RangeList) checkRangeElement(rangeElement [2]int) (start, end int, err error) {
	since, until := rangeElement[0], rangeElement[1]
	if until < since {
		return 0, 0, fmt.Errorf("%w: until is less than since", ErrInvalidRangeElement)
	}
	return since, until - 1, nil
}
